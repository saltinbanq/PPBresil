# PPBresil GitLab

## Description

## Documentation

The documentation overview is in the [readme in the doc directory](doc/README.md).

## Contributing

Please see the [contribution guidelines](CONTRIBUTING.md)

## Installation

Rien à faire, tu dl les fichiers et tu ouvre lance le .html.

## Utilisation

C'est là que ça devient intéressant, en effet pour ceux qui ne connaissent pas c'est tout simple:
TOUT marche par section, donc pour créer une slide c'est:
<section>
    <h1>Page1</h1>
</section>
<section>
    <h1>Page2</h1>
</section>
Ces slides seront donc disposée de gauche a droite (comme dans un ppx).
LA ou c'est intéressant, c'est pour rajouter des "sous slides", non pas de gauche a droite mais de haut en bas.
pour ce faire:
<section>
    <section>
        <h1>Sous-Page1</h1>
    </section>
    <section>
        <h1>Sous-Page2</h1>
    </section>
</section>

Pour ajouter du texte, images, vidéos etc... c'est du html j'vais pas vous donner un cours.

Pensez à ne surtout pas toucher au fichier css et js. On gère tout directement dans le .html.